﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurs2
{
    class Mark
    {
        //Маркер, отображающий прошла ли конфигурация отбор
        private bool _marker;
        public string Marker
        {
            get
            {
                if (_marker) return "V";
                else return " ";
            }
        }
        //оценки конфигурации
        private int[] marks = new int[4];

        public int this[string mark]
        {
            get
            {
                switch (mark)
                {
                    case "P1":return marks[0];
                    case "P2": return marks[1];
                    case "P3": return marks[2];
                    case "P4": return marks[3];
                    default: return -1488;
                }
            
            }
            set
            {
                if (value < 0) value = 0;
                if (value >100) value = 100;
                switch (mark)
                {
                    case "P1": marks[0]=value;break;
                    case "P2": marks[1]=value;break;
                    case "P3": marks[2]=value;break;
                    case "P4": marks[3]=value;break;
                }
            }            
        }

        public Mark(int p1, int p2, int p3, int p4)
        {
            marks[0] = p1;
            marks[1] = p2;
            marks[2] = p3;
            marks[3] = p4;
        }
        //проверка оценок на прохождение критерия
        public void CheckMarker(int valueCr)
        {
            _marker = (marks[0] >= valueCr) && (marks[1] >= valueCr) && (marks[2] >= valueCr) && (marks[3] >= valueCr);
        }
    }
}
