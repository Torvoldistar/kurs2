﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kurs2
{
    public partial class Form1 : Form
    {
        private char[] RowName;
        private List<MySystem> systems;
        private int k = 0;
        private int Value;

        //конструктор формы
        public Form1()
        {
            systems = new List<MySystem>();
            RowName = new char[1] {'A'};
            InitializeComponent();
            button6.Enabled = false;
            for (int i = 1; i < 5; i++) //инициализация заголовков столбцов dataGridView
            {
                dataGridView1.Columns.Add($"P{i}", $"P{i}");
                dataGridView2.Columns.Add($"P{i}", $"P{i}");
            }

            dataGridView1.Columns.Add("Check", "Отбор");
            dataGridView2.Columns.Add("Check", "Отбор");
            dataGridView1.RowHeadersWidth = 50;
            dataGridView2.RowHeadersWidth = 50;
            for (int i = 0; i < 5; i++)
            {
                button3_Click(this, null);
            }

        }


        //Проверка оценок
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Value = int.Parse(textBox1.Text);
            }
            catch
            {
                Value = 0;
                textBox1.Text = "0";
            }

            for (int i = 0; i < k; i++)
            {
                foreach (DataGridViewColumn Column in dataGridView1.Columns)
                {
                    try
                    {
                        systems[i].SecurityMark[Column.Name] =
                            int.Parse(dataGridView1[Column.Name, i].Value.ToString());
                        dataGridView1[Column.Name, i].Value = systems[i].SecurityMark[Column.Name];
                        systems[i].PerformanceMark[Column.Name] =
                            int.Parse(dataGridView2[Column.Name, i].Value.ToString());
                        dataGridView2[Column.Name, i].Value = systems[i].PerformanceMark[Column.Name];
                    }
                    catch
                    {
                        dataGridView1[Column.Name, i].Value = 0;
                        systems[i].SecurityMark[Column.Name] = 0;
                        dataGridView2[Column.Name, i].Value = 0;
                        systems[i].PerformanceMark[Column.Name] = 0;
                    }

                    if (Column.Name == "Check")
                    {
                        systems[i].SecurityMark.CheckMarker(Value);
                        systems[i].PerformanceMark.CheckMarker(Value);
                        dataGridView1[Column.Name, i].Value = $"{systems[i].SecurityMark.Marker}";
                        dataGridView2[Column.Name, i].Value = $"{systems[i].PerformanceMark.Marker}";
                        break;
                    }
                }

            }
        }

        //загрузка контрольного примера
        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "70";
            Value = 70;
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();
            k = 0;
            RowName[0] = 'A';
            AddRow(100, 70, 60, 90, 50, 50, 40, 40);
            AddRow(80, 40, 70, 100, 60, 40, 40, 90);
            AddRow(60, 90, 80, 90, 70, 60, 50, 60);
            AddRow(80, 70, 70, 80, 40, 30, 30, 30);
            AddRow(70, 50, 70, 90, 70, 70, 80, 70);
        }

        //добавить строку
        private void button3_Click(object sender, EventArgs e)
        {
            AddRow(0, 0, 0, 0, 0, 0, 0, 0);
        }

        //удалить последнюю строку
        private void button4_Click(object sender, EventArgs e)
        {
            if (k > 0)
            {
                k--;
                dataGridView1.Rows.RemoveAt(k);
                dataGridView2.Rows.RemoveAt(k);
                systems.RemoveAt(k);
                RowName[0]--;
            }
        }

        //заполнение строки конфигурации оценками
        private void AddRow(int p1, int p2, int p3, int p4, int r1, int r2, int r3, int r4)
        {
            dataGridView1.Rows.Add(p1, p2, p3, p4);
            dataGridView2.Rows.Add(r1, r2, r3, r4);
            dataGridView1.Rows[k].HeaderCell.Value = new string(RowName);
            dataGridView2.Rows[k].HeaderCell.Value = new string(RowName);
            systems.Add(new MySystem(new Mark(p1, p2, p3, p4), new Mark(r1, r2, r3, r4), RowName[0].ToString()));
            dataGridView1[4, k].ReadOnly = true;
            dataGridView2[4, k].ReadOnly = true;
            RowName[0]++;
            k++;
        }

        //произведение селекции по двум таблицам
        private void button5_Click(object sender, EventArgs e)
        {
            button2_Click(this, null);
            switch (button5.Text)
            {
                case "Произвести селекцию":
                    button5.Text = "Показать все";
                    var selector = new Selector(systems);
                    selector.select();

                    for (int i = 0; i < k; i++)
                    {
                        if (selector.BadSystems.Contains(systems[i]))
                        {
                            dataGridView1.Rows[i].Visible = false;
                            dataGridView2.Rows[i].Visible = false;
                        }
                    }

                    break;
                case "Показать все":
                    for (int i = 0; i < k; i++)
                    {
                        dataGridView1.Rows[i].Visible = true;
                        dataGridView2.Rows[i].Visible = true;
                    }

                    button5.Text = "Произвести селекцию";
                    break;
            }

            button6.Enabled = true;
        }

        //запись в файл
        private void button6_Click(object sender, EventArgs e)
        {
            RowName[0] = 'A';
            Stream myStream;
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 2;
            saveFileDialog.RestoreDirectory = true;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog.OpenFile()) != null)
                {
                    StreamWriter sw = new StreamWriter(myStream);
                    sw.WriteLine("Конфигурации, подходящие по критерию:");
                    for (int i = 0; i < k; i++)
                    {
                        if ((systems[i].PerformanceMark.Marker == "V") && (systems[i].SecurityMark.Marker == "V"))
                        {
                            sw.WriteLine($"{RowName[0]}: " +
                                         $"P1={systems[i].SecurityMark["P1"]} P2={systems[i].SecurityMark["P2"]} P3={systems[i].SecurityMark["P3"]} P4={systems[i].SecurityMark["P4"]} " +
                                         $"R1={systems[i].PerformanceMark["P1"]} R2={systems[i].PerformanceMark["P2"]} R3={systems[i].PerformanceMark["P3"]} R4={systems[i].PerformanceMark["P4"]}");
                        }

                        RowName[0]++;

                    }

                    sw.Flush();
                    myStream.Close();
                }
            }
        }
    }
}


    
