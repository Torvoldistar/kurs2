﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurs2
{
    class Selector
    {
        public List<MySystem> Systems { get; set; }
        public List<MySystem> BadSystems { get; set; }

        public Selector(List<MySystem> systems)
        {
            Systems = systems;
        }

        public void select()
        {
            BadSystems = Systems.Where(system =>
                system.SecurityMark.Marker == " " || system.PerformanceMark.Marker == " ").ToList();
        }
    }
}
