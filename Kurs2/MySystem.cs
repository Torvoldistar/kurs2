﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurs2
{
    class MySystem
    {
        public string Name { get; set; }
        public Mark SecurityMark { get; set; }
        public Mark PerformanceMark { get; set; }

        public MySystem(Mark securityMark, Mark performanceMark, string name)
        {
            SecurityMark = securityMark;
            PerformanceMark  = performanceMark;
            Name = name;
        }
    }
}
